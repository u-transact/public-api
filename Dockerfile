FROM ruby:2.4.2-slim

MAINTAINER Julio Telan <julio.telan@shoresuite.com>

ENV RACK_ENV=production
RUN apt-get update && apt-get install -y build-essential
RUN bundle config --global frozen 1
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY Gemfile /usr/src/app/
COPY Gemfile.lock /usr/src/app/
RUN bundle install --without development test
RUN mkdir config
COPY config/examples/mongoid.yml.deploy config/mongoid.yml
COPY config/settings/production.yml config/settings.yml
COPY db /usr/src/app/db
COPY config.ru /usr/src/app/
COPY transact_script.rb /usr/src/app/
COPY app /usr/src/app/app

EXPOSE 9292

CMD ["rackup", "--host", "0.0.0.0", "-p", "9292"]
