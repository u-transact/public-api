# !/usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH << 'app'
$LOAD_PATH.unshift File.expand_path('../app/models', __FILE__)

require 'rubygems'
require 'bundler/setup'
ENV['RACK_ENV'] = 'production'
require 'utransact'
require 'utransact/transaction'
require 'utransact/branch'
require 'date'
require 'mongoid'
require 'redis'
require 'active_support/concern'

$stderr.puts 'Creating a redis client'
client = Redis.new(Settings.redis.to_h)

branches = Utransact::Branch.all

$stderr.puts 'CHANGING THE QUEUED STATUS'
branches.each do |branch|
  date = DateTime.now.iso8601
  queued = client.get("queued_#{branch.code}_#{date}")
  current = client.get("current_queued_#{branch.code}_#{date}")
  next unless (queued.to_i - current.to_i).zero?
  transaction = Utransaction::Transaction.where(number: queued, branch_code: branch.code).first
  client.incr("current_queued_#{branch.code}_#{date}")
  next if transaction
  transaction.status = 'transacting'
  transaction.save
  sleep(rand(30))
  transaction.status = 'done'
  transaction.save
end
