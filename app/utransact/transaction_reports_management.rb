# frozen_string_literal: true

require 'grape'

require 'utransact/transaction'
require 'utransact/branch'

# U-transact Endpoint for Transaction
module Utransact
  # The Transaction API, using Grape
  class TransactionReportsManagement < Grape::API
    version 'v1'
    format :json
    prefix 'api'

    namespace :transactions do
      resource :reports do
        resource :stats do
          get do
            results = Utransact::Transaction.generate_per_transaction_stats
            { data: { stats_per_category: results[0], overall_stats: results[1] } }
          end
        end

        resource :hourly do
          get do
            result = Utransact::Transaction.hourly
            { data: result }
          end
        end
      end
    end
  end
end
