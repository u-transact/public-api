# frozen_string_literal: true

require 'grape'

require 'utransact/transaction'

# U-transact Endpoint for Transaction
module Utransact
  # The Transaction API, using Grape
  # rubocop:disable Metrics/ClassLength
  class TransactionManagement < Grape::API
    # rubocop:enable Metrics/ClassLength
    version 'v1'
    format :json
    prefix 'api'

    # rubocop:disable Metrics/BlockLength
    namespace :transactions do
      # rubocop:enable Metrics/BlockLength

      resource :live_queues do
        params do
          optional :page, type: Integer
        end
        get do
          page = params.include?('page') ? params[:page] : 1
          result = Utransact::Branch.live_queues(page)
          { data: result[0], links: result[1] }
        end
      end

      params do
        requires :user_id, type: String, desc: 'user id', allow_blank: false
      end
      get do
        query = {
          user_id: params[:user_id],
          status: { :$nin => %w[done cancelled] }
        }
        transaction = Utransact::Transaction.where(query).first
        data = if transaction
                 transaction.as_json
               else
                 []
               end
        { data: data }
      end

      params do
        requires :user_id, type: String, desc: 'user id', allow_blank: false
        requires :branch_code, type: String, desc: 'Branch Code', allow_blank: false
        requires :actions, type: Array, desc: 'Transaction Actions', allow_blank: false
      end
      post do
        check = Utransact::Transaction.where(user_id: params[:user_id], status: { :$nin => %w[done cancelled] }).first
        error!({ errors: 'You have an existing transaction' }, 400) if check
        transaction = Utransact::Transaction.create! branch_code: params[:branch_code], user_id: params[:user_id]
        error!({ errors: { max_transaction: 5 } }, 400) if params[:actions].length > 5
        params[:actions].each do |action|
          transaction.actions.create! amount: action['amount'], type: action['type'],
                                      account_number: action['account_number']
        end
      end

      # rubocop:disable Metrics/BlockLength
      route_param :user_id do
        # rubocop:enable Metrics/BlockLength
        resource :add do
          params do
            requires :user_id, type: String, desc: 'user id', allow_blank: false
            requires :actions, type: Array, desc: 'Transaction Actions', allow_blank: false
          end
          post do
            params_action = params[:actions].length
            error!({ errors: { max_transaction: 5 } }, 400) if params_action > 5
            query = { user_id: params[:user_id], status: { :$nin => %w[done cancelled] } }
            transaction = Utransact::Transaction.where(query).first
            error!({ errors: 'Not Found' }, 404) unless transaction
            error!({ errors: 'Can only add when queued' }, 404) unless transaction.status == 'queued'
            actions_length = transaction.actions.length
            error!({ errors: { max_transaction: 5 } }, 400) if actions_length + params_action > 5 || actions_length > 5
            params[:actions].each do |action|
              transaction.actions.create! amount: action['amount'], type: action['type'],
                                          account_number: action['account_number']
            end
          end
        end

        resource :cancel do
          params do
            requires :user_id, type: String, desc: 'user id', allow_blank: false
          end
          post do
            query = { user_id: params[:user_id], status: { :$nin => %w[done cancelled] } }
            transaction = Utransact::Transaction.where(query).first
            error!({ errors: 'Not Found' }, 404) unless transaction
            transaction.status = 'cancelled'
            transaction.save
          end
        end

        resource :process do
          params do
            requires :user_id, type: String, desc: 'user id', allow_blank: false
          end
          post do
            query = { user_id: params[:user_id], status: { :$nin => %w[done cancelled] } }
            transaction = Utransact::Transaction.where(query).first
            error!({ errors: 'Not Found' }, 404) unless transaction
            transaction.status = 'transacting'
            transaction.save
          end
        end

        resource :finalize do
          post do
            query = { user_id: params[:user_id], status: { :$nin => %w[done cancelled] } }
            transaction = Utransact::Transaction.where(query).first
            error!({ errors: 'Not Found' }, 404) unless transaction
            error = { errors: 'Can only transition from transacting to done' }
            error!(error, 400) if transaction.status != 'transacting'
            transaction.status = 'done'
            transaction.save
            redis = Redis.new(Settings.redis.to_h)
            date = transaction.created_at.strftime('%Y-%m-%d')
            redis.incr('current_queued_' + transaction.branch_code.to_s + '_' + date)
          end
        end

        params do
          requires :user_id, type: String, desc: 'user id', allow_blank: false
        end
        get do
          query = { user_id: params[:user_id], status: { :$nin => %w[done cancelled] } }
          transaction = Utransact::Transaction.where(query).first
          data = if transaction
                   transaction.as_json
                 else
                   []
                 end
          { data: data }
        end
      end
    end
  end
end
