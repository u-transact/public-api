# frozen_string_literal: true

require 'grape'

require 'utransact/branch'

# U-transact Endpoint for Branches
module Utransact
  # The Branch API, using Grape
  class BranchManagement < Grape::API
    version 'v1'
    format :json
    prefix 'api'

    # rubocop:disable Metrics/BlockLength
    namespace :branches do
      # rubocop:enable Metrics/BlockLength
      params do
        optional :latitude, type: Float
        optional :longitude, type: Float
        optional :search, type: String
        optional :page, type: Integer
        optional :type, type: String
      end
      # rubocop:disable Metrics/BlockLength
      get do
        # rubocop:enable Metrics/BlockLength
        if params.include?('latitude') && params.include?('longitude') &&
           params[:type] == 'nearest'
          { data: Utransact::Branch.find_nearest(params[:latitude], params[:longitude]) }
        elsif params.include?('latitude') && params.include?('longitude') &&
              params[:type] == 'fastest'
          { data: Utransact::Branch.find_fastest(params[:latitude], params[:longitude]) }
        elsif params.include?('search')
          page = if params.include?('page')
                   params[:page]
                 else
                   1
                 end
          branches = Utransact::Branch.search(params[:search])
          last_page = [(Utransact::Branch.count + 20) / 20, 1].max
          next_page = last_page > page + 1 ? page + 1 : last_page
          links = {
            first: "/api/v1/branches?search=#{params[:search]}&page=1",
            next: "/api/v1/branches?search=#{params[:search]}&page=#{next_page}",
            last: "/api/v1/branches?search=#{params[:search]}&page=#{last_page}"
          }
          data = branches.skip(20 * (page - 1)).limit(20).map do |branch|
            Utransact::Branch.format_element(branch, nil)
          end.map(&:as_json)
          { data: data, links: links }
        else
          page = if params.include?('page')
                   params[:page]
                 else
                   1
                 end
          last_page = [(Utransact::Branch.count + 20) / 20, 1].max
          next_page = last_page > page + 1 ? page + 1 : last_page
          links = {
            first: '/api/v1/branches?page=1',
            next: "/api/v1/branches?page=#{next_page}",
            last: "/api/v1/branches?page=#{last_page}"
          }
          data = Utransact::Branch.all.skip(20 * (page - 1)).limit(20).map do |branch|
            Utransact::Branch.format_element(branch, nil)
          end.map(&:as_json)
          { data: data, links: links }
        end
      end
    end
  end
end
