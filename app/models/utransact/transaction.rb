# frozen_string_literal: true

require 'money'
require 'redis'
require 'date'

# The Utransact module
module Utransact
  # An transaction
  # rubocop:disable Metrics/ClassLength
  class Transaction
    # rubocop:enable Metrics/ClassLength
    include Mongoid::Document
    include Mongoid::Timestamps
    include Utransact::MongoidHelper
    include Utransact::JsonApiHelper

    field :branch_code, type: String
    validates_presence_of :branch_code
    field :number, type: Integer
    field :user_id, type: String
    validates_presence_of :user_id
    VALID_STATUSES = %w[queued transacting done cancelled].freeze
    # Valid states are queued, transacting, cancelled and done
    field :status, default: 'queued'
    validates :status, inclusion: { in: VALID_STATUSES }

    has_many :actions, class_name: 'Utransact::Action'

    view :by_user_id, key: :user_id
    view :status, key: :status

    before_create :generate_transaction_number

    PUBLIC_ATTRIBUTES = %w[branch_code number user_id status].freeze

    def as_json(*)
      new_attributes = Hash[attributes.slice(*PUBLIC_ATTRIBUTES)]
      new_attributes[:actions] = actions
      {
        id: id.to_s,
        type: 'Transaction',
        attributes: new_attributes
      }
    end

    # rubocop:disable Metrics/AbcSize
    def generate_transaction_number
      # rubocop:enable Metrics/AbcSize
      redis = Redis.new(Settings.redis.to_h)
      date = created_at.strftime('%Y-%m-%d')
      count = redis.incr('queued_' + branch_code.to_s + '_' + date)
      current = redis.get('current_queued_' + branch_code.to_s + '_' + date)
      redis.incr('current_queued_' + branch_code.to_s + '_' + date) unless current
      generated_number = format('%04d', count)
      self.number = generated_number.to_s
    end

    class << self
      # Returns an Array of the transaction stats and the total amount for that date
      # rubocop:disable Metrics/AbcSize
      # rubocop:disable Metrics/MethodLength
      def generate_per_transaction_stats
        # rubocop:enable Metrics/AbcSize
        # rubocop:enable Metrics/MethodLength
        default_value = {
          payment: { total: 0.0, ave_queue_time: 0, total_queue_time: 0, total_transactions: 0 },
          withdrawal: { total: 0.0, ave_queue_time: 0, total_queue_time: 0, total_transactions: 0 },
          encashment: { total: 0.0, ave_queue_time: 0, total_queue_time: 0, total_transactions: 0 },
          deposit: { total: 0.0, ave_queue_time: 0, total_queue_time: 0, total_transactions: 0 }
        }
        total_amount = 0.0
        query = { :created_at.gte => Date.today, status: 'done' }
        transactions = Utransact::Transaction.where(query).each_with_object(default_value) do |cur, acc|
          cur.actions.each do |action|
            acc[action.type.to_sym][:total] += action.amount.to_f
            acc[action.type.to_sym][:total_transactions] += 1
            acc[action.type.to_sym][:total_queue_time] += (cur.updated_at - cur.created_at)
            if action[:type] == 'withdrawal' || action[:type] == 'encashment'
              total_amount -= action.amount.to_f
            else
              total_amount += action.amount.to_f
            end
          end
        end
        total_queue_time = 0.0
        total_transactions = 0
        default_value.keys.each do |key|
          object = default_value[key.to_sym]
          total_queue_time += object[:total_queue_time].to_f
          total_transactions += object[:total_transactions]
          value = object[:total_queue_time].to_f / object[:total_transactions].to_f
          object[:ave_queue_time] = if object[:total_transactions] != 0.0
                                      value
                                    else
                                      0
                                    end
        end
        total_average_queue_time = total_queue_time / total_transactions.to_f
        overall_stats = {
          total_transactions: total_transactions,
          total_average_queue_time: total_average_queue_time,
          total_amount: total_amount
        }
        [transactions, overall_stats]
      end

      # rubocop:disable Metrics/AbcSize
      # rubocop:disable Metrics/MethodLength
      def compute_total_today
        # rubocop:enable Metrics/AbcSize
        # rubocop:enable Metrics/MethodLength
        total_amount = 0.0
        total_transactions = 0
        total_queue_time = 0.0
        query = { :created_at.gte => Date.today, status: 'done' }
        Utransact::Transaction.where(query).each do |cur|
          cur.actions.each do |action|
            total_queue_time += (cur.updated_at - cur.created_at)
            if action[:type] == 'withdrawal' || action[:type] == 'encashment'
              total_amount -= action.amount.to_f
            else
              total_amount += action.amount.to_f
            end
            total_transactions += 1
          end
        end
        total_average_queue_time = total_queue_time / total_transactions.to_f
        redis = Redis.new(Settings.redis.to_h)
        redis.incrbyfloat('total_amount', total_amount)
        redis.incrbyfloat('total_average_queue_time', total_average_queue_time)
        redis.incrbyfloat('total_transactions', total_transactions)
        {
          total_transactions: total_transactions,
          total_average_queue_time: total_average_queue_time,
          total_amount: total_amount
        }
      end

      # rubocop:disable Metrics/AbcSize
      def hourly
        # rubocop:enable Metrics/AbcSize
        hour_step = (1.to_f / 24)
        date = Date.today
        date_time = date.to_datetime
        date_time_limit = date_time + 1

        date_time.step(date_time_limit, hour_step).first(24).map do |new_date|
          amount = Utransact::Transaction.query_hourly(new_date.change(offset: '+8')).reduce(0.0) do |acc, cur|
            cur.actions.each do |action|
              if action.type == 'withdrawal' || action.type == 'encashment'
                acc -= action.amount.to_f
              else
                acc += action.amount.to_f
              end
            end
            acc
          end
          {
            hour: new_date.strftime('%H:%M'),
            amount: amount
          }
        end
      end

      def query_hourly(date_time)
        query = {
          :created_at.gt => (date_time - 1.hour).iso8601, :created_at.lte => date_time.iso8601, status: 'done'
        }
        Utransact::Transaction.where(query)
      end
    end
  end

  # Action Model
  class Action
    include Mongoid::Document
    include Mongoid::Timestamps
    include Utransact::MongoidHelper
    include Utransact::JsonApiHelper

    belongs_to :transaction, class_name: 'Utransact::Transaction'
    PUBLIC_ATTRIBUTES = %w[amount type account_number].freeze
    def as_json(*)
      {
        id: id.to_s,
        type: 'Action',
        attributes: Hash[attributes.slice(*PUBLIC_ATTRIBUTES)]
      }
    end

    VALID_TYPES = %w[deposit payment withdrawal encashment].freeze
    field :type, type: String
    validates :type, inclusion: { in: VALID_TYPES }

    field :amount, type: Money
    field :account_number, type: String
  end
end
