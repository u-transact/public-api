# frozen_string_literal: true

require 'google_maps_service'
require 'redis'

# The Utransact module
module Utransact
  # A branch class
  class Branch
    include Mongoid::Document
    include Mongoid::Timestamps
    include Utransact::MongoidHelper

    field :name, type: String, default: ''
    field :code, type: String
    field :address, type: String, default: ''
    field :latitude, type: Float
    field :longitude, type: Float
    field :contact_number, type: String
    field :email, type: String

    view :by_name, key: :name
    view :by_code, key: :code

    PUBLIC_ATTRIBUTES = %w[name code email address latitude longitude contact_number].freeze
    def as_json(*)
      {
        id: id.to_s,
        type: 'Branch',
        attributes: Hash[attributes.slice(*PUBLIC_ATTRIBUTES)]
      }
    end

    class << self
      def search(terms)
        criteria = %i[name address].map do |key|
          terms.split.map { |term| { key => Regexp.new(Regexp.escape(term), true) } }
        end.flatten
        Branch.or(criteria)
      end

      # Returns the nearest 10 branches
      def find_nearest(lat, long)
        get_distance(lat, long).sort_by { |branch| branch[:value].to_i }.reverse.first(10)
      end

      # Returns the nearest 10 branches
      def find_fastest(lat, long)
        get_distance(lat, long).sort_by { |branch| branch[:difference].to_i }.first(10)
      end

      # rubocop:disable Metrics/AbcSize
      def format_element(branch, dis)
        # rubocop:enable Metrics/AbcSize
        redis = Redis.new(Settings.redis.to_h)
        date = DateTime.now.strftime('%Y-%m-%d')
        queued = redis.get("queued_#{branch.code}_#{date}")
        current = redis.get("current_queued_#{branch.code}_#{date}")
        difference = queued.to_i - current.to_i
        current_queue = check_if_empty(current)
        check_queued = check_if_empty(queued)
        data = { name: branch.name, code: branch.code, address: branch.address, current_queue: current_queue,
                 queued: check_queued, difference: difference }
        if dis
          data[:text] = dis[:text]
          data[:value] = dis[:value]
        end
        data
      end

      def get_distance(lat, long)
        gmaps = GoogleMapsService::Client.new(
          key: ENV['GCLOUD_API_KEY'],
          retry_timeout: 20,
          queries_per_second: 10
        )
        origin = [lat, long]
        Utransact::Branch.all.only(:latitude, :longitude, :name, :code, :address).map do |branch|
          matrix = gmaps.distance_matrix(origin, [branch.latitude, branch.longitude],
                                         mode: 'driving',
                                         units: 'metric')
          dis = matrix[:rows][0][:elements][0][:distance]
          format_element(branch, dis)
        end
      end

      def check_if_empty(value)
        if value
          value
        else
          0
        end
      end

      def live_queues(page)
        last_page = [(Utransact::Branch.count + 20) / 20, 1].max
        next_page = last_page > page + 1 ? page + 1 : last_page
        links = {
          first: '/api/v1/transactions/live_queues?page=1',
          next: "/api/v1/transactions/live_queues?page=#{next_page}",
          last: "/api/v1/transactions/live_queues?page=#{last_page}"
        }
        data = Utransact::Branch.all.skip(20 * (page - 1)).limit(20).map do |branch|
          Utransact::Branch.format_element(branch, nil)
        end
        [data, links]
      end
    end
  end
end
