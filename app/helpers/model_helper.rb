# frozen_string_literal: true

# Compilation of Model Helper
module ModelHelper
  def check_if_empty(list)
    if list.empty?
      []
    else
      list.map(&:to_json)
    end
  end
end
