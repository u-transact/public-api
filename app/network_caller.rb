# frozen_string_literal: true

require 'faraday'

# A reusable helper class for dealing with network calls outside the API
class NetworkCaller
  class << self
    def list_branches
      conn = Faraday.new(url: 'https://api-uat.unionbankph.com/partners/sb/locators/v1/branches',
                         headers: { 'Accept' => 'application/json',
                                    'x-ibm-client-id' => ENV['UB_CLIENT_ID'],
                                    'x-ibm-client-secret' => ENV['UB_CLIENT_SECRET'] })
      resp = conn.get
      JSON.parse(resp.body)
    end
  end
end
