# frozen_string_literal: true

require 'config'
require 'mongoid'
require 'money'
require 'active_support/concern'

RACK_ENV = ENV['RACK_ENV'] || 'production'

Config.load_and_set_settings(Config.setting_files(File.expand_path('../../config', __FILE__), RACK_ENV))

CONFIG_MONGOID_YML = File.expand_path('../../config/mongoid.yml', __FILE__)
CONFIG_MONGOID_YML_EXISTS = File.exist?(CONFIG_MONGOID_YML)
if CONFIG_MONGOID_YML_EXISTS
  Mongoid.load! CONFIG_MONGOID_YML
else
  abort %(Unable to initialize Mongoid: "#{CONFIG_MONGOID_YML}" does not exist!)
end

Money.default_currency = Money::Currency.new('PHP')

module Utransact
  # Helper module for Mongoid
  module MongoidHelper
    extend ActiveSupport::Concern

    class_methods do
      def infer_key(name, opts = {})
        if opts.key?(:key)
          [opts.delete(:key)].flatten
        elsif opts.key?(:keys)
          [opts.delete(:keys)].flatten
        elsif name =~ /^by_([[:alpha:]_]+)$/ # see https://ruby-doc.org/core-2.1.1/Regexp.html
          captured = Regexp.last_match(1)
          captured.index('_and_') ? captured.split('_and_') : [captured]
        else
          raise "No key specified for view :#{name}!"
        end
      end
      private :infer_key

      # Allow for easily creating `find_all_by_*` and `find_by_*` helper methods
      def view(name, opts = {})
        keys = infer_key(name, opts)
        define_singleton_method("find_all_#{name}".to_sym) do |*args|
          where(Hash[keys.zip(args.map { |a| Regexp.new(Regexp.escape(a), true) })])
        end
        define_singleton_method("find_#{name}".to_sym) do |*args|
          send("find_all_#{name}".to_sym, *args).first
        end
      end
    end
  end

  # JsonHelper
  module JsonApiHelper
    extend ActiveSupport::Concern

    included do
      def attributes_hash
        self.class.public_fields.each_with_object({}) do |f, h|
          sym = f.to_sym
          val = send(sym)
          next unless val
          h[sym] = case val
                   when Date
                     val.iso8601
                   else
                     val
                   end
        end
      end

      def as_json(*)
        {
          type: self.class.name.demodulize,
          id: id.is_a?(BSON::ObjectId) ? id.to_s : id,
          attributes: attributes_hash
        }
      end

      def to_s
        data = attributes_hash.map { |k, v| "#{k}=#{v.inspect}" }.join(' ')
        "#<#{self.class}@#{id} #{data}>"
      end
    end

    class_methods do
      def public_fields(*args)
        @public_fields = args.first.is_a?(Array) ? args.first : args unless args.empty?
        @public_fields || []
      end
    end
  end
end

# Time class
class Time
  def to_date
    Date.new(year, month, day)
  end
end
