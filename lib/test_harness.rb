# frozen_string_literal: true

require 'erb'
require 'uri'
require 'yaml'
require 'socket'

# TODO: Yes, I know, duplicate of CONFIG_MONGOID_YML in shore.rb
CONFIG_MONGOID_YML = File.expand_path('../../config/mongoid.yml', __FILE__)

puts "ENV['DOCKER_HOST'] => #{ENV['DOCKER_HOST']}"

config = YAML.safe_load(ERB.new(File.read(CONFIG_MONGOID_YML)).result)
puts "config => #{config.inspect}"

abort %("#{CONFIG_MONGOID_YML}" exists, but doesn't seem to be valid YAML!) unless config

abort '"RACK_ENV" is undefined' unless RACK_ENV

hosts = config.dig(RACK_ENV, 'clients', 'default', 'hosts')

puts "hosts => #{hosts.inspect}"

abort %("#{CONFIG_MONGOID_YML}" exists, but no '#{RACK_ENV}' configuration found!) unless hosts&.first

def host_port_listening?(host, port)
  Socket.tcp(host, port.to_i, connect_timeout: 5) { true }
rescue StandardError => _e
  false
end

mongo_host, mongo_port = *hosts.first.split(':')
mongo_is_listening = host_port_listening?(mongo_host, mongo_port)

puts "mongo_is_listening => #{mongo_is_listening}"

unless mongo_is_listening
  `docker-compose -f docker-compose.test.yml up -d`

  ENV['REDIS_TEST_URL'] = "redis://#{URI.parse(ENV['DOCKER_HOST'] || 'localhost').host}:6380"
  at_exit do
    `docker-compose -f docker-compose.test.yml down -v`
  end
end

unless ENV.key?('REDIS_TEST_URL')
  abort 'No REDIS_TEST_URL or DOCKER_HOST environment variable defined' unless ENV.key?('DOCKER_HOST')
  ENV['REDIS_TEST_URL'] = 'redis://localhost:6380'
end
