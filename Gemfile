# frozen_string_literal: true

# A sample Gemfile
source 'https://rubygems.org'

# Foundational
gem 'activesupport', '~> 5.0'
gem 'bcrypt', '~> 3.1.11'
gem 'check_digit', '~> 0.1.1'
gem 'config', '~> 1.4.0'
gem 'json', '~> 2.1.0'
gem 'money', '~> 6.9.0'
gem 'redis', '~>3.2'

# Persistence
gem 'active_model_serializers', '~> 0.9.6'
gem 'activemodel', '~> 5.0'
gem 'mongoid', '~> 6.2.1'

# Presentation
gem 'kaminari-mongoid', '~> 1.0.1'
gem 'kaminari-sinatra', '~> 1.0.1'

# Rack/Web
gem 'jwt', '~> 1.5.6'
gem 'rack', '~> 2.0'
gem 'sinatra', '~> 2.0.0'
gem 'sinatra-contrib', '~> 2.0.0'

gem 'grape', '~> 0.19.2'

gem 'faraday', '~> 0.9.2'

# GCloud Computation
gem 'google_maps_service', '~> 0.4.1'

group :development do
  gem 'rack-cors', '~> 0.4.1', require: 'rack/cors'
  gem 'rubocop', '~> 0.48.1', require: false
end

group :test do
  gem 'rainbow', '~> 2.2.2', require: false

  # Code coverage
  gem 'simplecov', '~> 0.14.1', require: false

  # Use cucumber & rspec for tests
  gem 'capybara', '~> 2.14.0', require: false
  gem 'cucumber', '~> 2.0', require: false
  gem 'cucumber-sinatra', '~> 0.5.0', require: false
  gem 'rspec', '~> 3.2', require: false

  # JSON path to help specify JSON tests
  gem 'jsonpath', '~> 0.5.8', require: false

  # WebMock and VCR to record HTTP interactions with other services
  gem 'httpclient', '~> 2.8.3', require: false
  gem 'vcr', '~> 3.0.3', require: false
  gem 'webmock', '~> 3.1.0', require: false
end
