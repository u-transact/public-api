# frozen_string_literal: true

$LOAD_PATH.unshift File.expand_path('../../app', __FILE__)
$LOAD_PATH.unshift File.expand_path('../../app/models', __FILE__)

require 'config'

Config.load_and_set_settings(File.expand_path('../../config/settings.yml', __FILE__))

require 'utransact'
require 'network_caller'
require 'utransact/branch'

$stderr.puts 'SEEDING THE DATA'
Utransact::Branch.unscoped.delete_all
branches = NetworkCaller.list_branches
branches.first(20).each do |branch|
  Utransact::Branch.create! code: branch['id'], name: branch['name'], address: branch['address'],
                            latitude: branch['latitude'], longitude: branch['longitude'],
                            contact_number: branch['contactNo'], email: branch['email']
end

$stderr.puts 'DONE'
