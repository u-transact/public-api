Feature: Transaction Reports Management
  In order to manage the transaction reports

  @transactions
  Scenario: Get Transaction Stat Reports
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |done  |deposit:22322:50000, withdrawal:22322:50000|
      |1132234|53         |done  |deposit:22322:50000                        |
      |1132235|53         |done  |deposit:22322:50000                        |
    When we do GET "/api/v1/transactions/reports/stats"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be a hash with 2 elements
    And the JSON "data.stats_per_category" should be a hash with 4 elements
    And the JSON "data.stats_per_category.deposit.total" should be 150000
    And the JSON "data.stats_per_category.deposit.total_transactions" should be 3
    And the JSON "data.stats_per_category.payment.total" should be 0
    And the JSON "data.stats_per_category.payment.total_transactions" should be 0
    And the JSON "data.stats_per_category.withdrawal.total" should be 50000
    And the JSON "data.stats_per_category.withdrawal.total_transactions" should be 1
    And the JSON "data.stats_per_category.encashment.total" should be 0
    And the JSON "data.stats_per_category.encashment.total_transactions" should be 0
    And the JSON "data.overall_stats" should be a hash with 3 elements
    And the JSON "data.overall_stats.total_transactions" should be 4
    And the JSON "data.overall_stats.total_amount" should be 100000

  @transactions
  Scenario: Get Hourly Transactions
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |done  |deposit:22322:50000, withdrawal:22322:50000|
      |1132234|53         |done  |deposit:22322:50000                        |
      |1132235|53         |done  |deposit:22322:50000                        |
    When we do GET "/api/v1/transactions/reports/hourly"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be an array with 24 elements
    And the JSON "data[0].hour" should be "00:00"
    And the JSON "data[0].amount" should be 0

  @branches @transactions @redis
  Scenario: Retrieve live queues of branches
    Given the following `Branch`s:
      |name   |code|latitude |longitude |email          |contact_number|address|
      |Baliwag|173 |14.953611|120.888889|test1@gmail.com|12312312      |baliwag|
      |Banaue |78  |14.623223|121.007997|test2@gmail.com|12312313      |banaue |
    And the queues for each branch is:
      |code|queued|current|
      |173 |100   |80     |
      |78  |100   |90     |
    When we do GET "/api/v1/transactions/live_queues"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload