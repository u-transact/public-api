# frozen_string_literal: true

require 'simplecov'
SimpleCov.start

if ENV.key?('RACK_ENV')
  RACK_ENV = ENV['RACK_ENV']
else
  ENV['RACK_ENV'] = RACK_ENV = 'test'
end

$LOAD_PATH.unshift File.expand_path('../../../app', __FILE__)
$LOAD_PATH.unshift File.expand_path('../../../app/models', __FILE__)

require 'capybara'
require 'capybara/cucumber'
require 'rspec'

require 'jsonpath'

require 'redis'

CUCUMBER_VERBOSE = ENV.key?('VERBOSE')

require 'test_harness'

require 'utransact'
require 'utransact/branch_management'
require 'utransact/transaction_management'
require 'utransact/transaction_reports_management'

apps = [Utransact::BranchManagement, Utransact::TransactionManagement, Utransact::TransactionReportsManagement]
Capybara.app = Rack::Cascade.new(apps)
# My world
class MyWorld
  include Capybara::DSL
  include RSpec::Expectations
  include RSpec::Matchers
end

World do
  MyWorld.new
end
