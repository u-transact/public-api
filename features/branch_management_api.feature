Feature: Branch Management
  In order to manage the branches

  @branches
  Scenario: Get all branches
    Given the following `Branch`s:
      |name   |code|latitude |longitude |email          |contact_number|address|
      |Baliwag|173 |14.953611|120.888889|test1@gmail.com|12312312      |baliwag|
      |Banaue |78  |14.623223|121.007997|test2@gmail.com|12312313      |banaue |
    And the queues for each branch is:
      |code|queued|current|
      |173 |100   |80     |
      |78  |100   |90     |
    When we do GET "/api/v1/branches"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be an array with 2 elements

  @branches
  Scenario: Search Branch with text "li"
    Given the following `Branch`s:
      |name   |code|latitude |longitude |email          |contact_number|address|
      |Baliwag|173 |14.953611|120.888889|test1@gmail.com|12312312      |baliwag|
      |Banaue |78  |14.623223|121.007997|test2@gmail.com|12312313      |banaue |
    And the queues for each branch is:
      |code|queued|current|
      |173 |100   |80     |
      |78  |100   |90     |
    When we do GET "/api/v1/branches?search=li"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be an array with 1 elements

  @branches
  Scenario: Get all the nearest branches
    Given the following `Branch`s:
      |name   |code|latitude |longitude |email          |contact_number|address|
      |Baliwag|173 |14.953611|120.888889|test1@gmail.com|12312312      |baliwag|
      |Banaue |78  |14.623223|121.007997|test2@gmail.com|12312313      |banaue |
    And the queues for each branch is:
      |code|queued|current|
      |173 |100   |80     |
      |78  |100   |90     |
    When we do GET "/api/v1/branches?type=nearest&latitude=14.589853&longitude=121.060939"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be an array with 2 elements
    And the JSON "data[0].code" should be "173"
    And the JSON "data[1].code" should be "78"

  @branches
  Scenario: Get all the fastest branches
    Given the following `Branch`s:
      |name   |code|latitude |longitude |email          |contact_number|address|
      |Baliwag|173 |14.953611|120.888889|test1@gmail.com|12312312      |baliwag|
      |Banaue |78  |14.623223|121.007997|test2@gmail.com|12312313      |banaue |
    And the queues for each branch is:
      |code|queued|current|
      |173 |100   |80     |
      |78  |100   |90     |
    When we do GET "/api/v1/branches?type=fastest&latitude=14.589853&longitude=121.060939"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data" should be an array with 2 elements
    And the JSON "data[0].code" should be "78"
    And the JSON "data[1].code" should be "173"
