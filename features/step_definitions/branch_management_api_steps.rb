# frozen_string_literal: true

require 'utransact/branch'

Before('@branch') do
  Utransact::Branch.unscoped.delete_all
end

And(/^the queues for each branch is:$/) do |table|
  redis = Redis.new(Settings.redis.to_h)
  date = DateTime.now.iso8601
  table.hashes.each do |row|
    redis.set("queued_#{row['code']}_#{date}", row['queued'])
    redis.set("current_queued_#{row['code']}_#{date}", row['current'])
  end
end
