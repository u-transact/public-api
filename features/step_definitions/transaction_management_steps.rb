# frozen_string_literal: true

require 'utransact/transaction'
require 'money'
require 'redis'

Before('@transactions') do
  Utransact::Transaction.unscoped.delete_all
end

Given(/^the following Transactions:$/) do |table|
  table.hashes.each do |row|
    transaction = Utransact::Transaction.create! branch_code: row['branch_code'],
                                                 user_id: row['user_id'], status: row['status']
    row['actions'].split(/\s*,\s*/).map do |s|
      parts = s.split(':')
      transaction.actions.create! type: parts[0],
                                  account_number: parts[1],
                                  amount: parts[2]
    end
  end
end

And(/^the number of transactions is now "([^"]*)"$/) do |existing_transaction|
  expect(Utransact::Transaction.where(status: { :$nin => %w[done cancelled] }).count).to eq(existing_transaction.to_i)
end

And(/^the transaction of "([^"]*)"'s status is now "([^"]*)"$/) do |id, status|
  transaction = Utransact::Transaction.find_by_user_id(id)
  expect(transaction.status).to eq(status)
end

And(/^the transaction of "([^"]*)"'s actions is now "([^"]*)"$/) do |id, actions|
  transaction = Utransact::Transaction.find_by_user_id(id)
  expect(transaction.actions.length).to eq(actions.to_i)
end
