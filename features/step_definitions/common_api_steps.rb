# frozen_string_literal: true

Before do
  # set common instance variables to defaults
  @browser ||= Rack::Test::Session.new(Rack::MockSession.new(Capybara.app))
  @browser.header 'Content-Type', 'application/json'
end

Before('@redis') do
  require 'redis'
  redis = Redis.new(Settings.redis.to_h)
  redis.flushall
end

def deep_interpolate(h)
  h.each do |k, v|
    case v
    when String
      # rubocop:disable Security/Eval
      vv = eval('"' + v + '"')
      # rubocop:enable Security/Eval
      h[k] = vv
    when Hash
      deep_interpolate(v)
    end
  end
end

Given(/^the following JSON payload:$/) do |payload|
  @payload = JSON.parse(payload)
  deep_interpolate(@payload)
  puts @payload if CUCUMBER_VERBOSE
end

Given(/^the "([^"]+)" header is "([^"]*)"$/) do |header, value|
  # rubocop:disable Security/Eval
  @browser.header header, eval('"' + value + '"')
  # rubocop:enable Security/Eval
end

Given(/^there is no "([^"]+)" header$/) do |header|
  @browser.header header, nil
end

When(/^we do GET "([^"]*)"$/) do |path|
  # rubocop:disable Security/Eval
  path = eval(%("#{path}")) if path.match?(/\#{[^}]+}/)
  # rubocop:enable Security/Eval
  puts "GET #{path}" if CUCUMBER_VERBOSE
  if @payload

    @browser.get path, @payload.to_json
  else
    @browser.get path
  end
end

When(/^we do POST "([^"]*)"$/) do |path|
  # rubocop:disable Security/Eval
  path = eval(%("#{path}")) if path.match?(/\#{[^}]+}/)
  # rubocop:enable Security/Eval
  puts "POST #{path}" if CUCUMBER_VERBOSE
  @browser.post path, @payload.to_json
end

When(/^we do PUT "([^"]*)"$/) do |path|
  # rubocop:disable Security/Eval
  path = eval(%("#{path}")) if path.match?(/\#{[^}]+}/)
  # rubocop:enable Security/Eval
  puts "PUT #{path}" if CUCUMBER_VERBOSE
  @browser.put path, @payload.to_json
end

When(/^we do DELETE "([^"]*)"$/) do |path|
  # rubocop:disable Security/Eval
  path = eval(%("#{path}")) if path.match?(/\#{[^}]+}/)
  # rubocop:enable Security/Eval
  if @payload
    @browser.delete path, @payload.to_json
  else
    @browser.delete path
  end
end

Then(/^(?:the )?response status should be "(\S+) \(([^)]+)\)"$/) do |code, _status|
  expect(@browser.last_response.status).to eq(code.to_i)
end

Then(/^(?:the )?response status should be (OK|Unauthorized)$/) do |code|
  expect(case code
         when 'OK'
           @browser.last_response.ok?
         when 'Unauthorized'
           @browser.last_response.unauthorized?
         end).to be true
end

Then(/^the content type header should be "([^"]*)"$/) do |val|
  expect(@browser.last_response.headers['Content-Type']).to eq(val)
end

Then(/^(?:the )?response "([^"]+)" header should match %r\{([^}]+)}$/) do |header, pattern|
  val = @browser.last_response.headers[header]
  expect(val).to match(pattern)
end

Then(/^(?:the )?response "([^"]*)" header should contain a URL$/) do |key|
  expect(@browser.last_response.header.key?(key)).to be true
  value = @browser.last_response.header[key]
  expect { @url = URI.parse(value) }.to_not raise_error
end

Then(/^that URL should match %r{([^}]+)}$/) do |pattern|
  expect(@url.to_s).to match(Regexp.new(pattern))
end

Then(/^that URL should end with "(\S+)"$/) do |s|
  expect(@url.to_s.end_with?(s)).to be true
end

Then(/^(?:the )?response body should contain a JSON payload$/) do
  expect { @json = JSON.parse(@browser.last_response.body) }.to_not raise_error
  puts JSON.pretty_generate(@json)
end

Then(/^the JSON "([^"]*)" should be "([^"\\]*(?:\\.[^"\\]*)*)"$/) do |jsonpath, expected|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect(actual).to eq(expected.gsub('\"', '"'))
end

Then(/^the JSON "([^"]*)" should be `([^"\\]*(?:\\.[^"\\]*)*)`$/) do |jsonpath, expected|
  actual = JsonPath.on(@json, jsonpath)[0]
  # rubocop:disable Security/Eval
  expect(actual).to eq(eval(expected))
  # rubocop:enable Security/Eval
end

Then(%r{^the JSON "([^"]*)" should match /([^/]+)/$}) do |jsonpath, pattern|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect(actual).to match(Regexp.new(pattern))
end

Then(/^the JSON "([^"]*)" should be (\d+)$/) do |jsonpath, expected_number|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect(actual).to eq(expected_number.to_i)
end

Then(/^the JSON "([^"]*)" should be bool "([^"\\]*(?:\\.[^"\\]*)*)"$/) do |jsonpath, expected_bool|
  actual = JsonPath.on(@json, jsonpath)[0]
  # rubocop:disable Security/Eval
  expect(actual).to eq(eval(expected_bool))
  # rubocop:enable Security/Eval
end

Then(/^the JSON "([^"]*)" should be a URL ending with "([^"]+)"$/) do |jsonpath, path|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect { @url = URI.parse(actual) }.to_not raise_error
  expect(actual.end_with?(path)).to be true
end

Then(%r{^the JSON "([^"]*)" should be a URL matching /([^/]+)/$}) do |jsonpath, pattern|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect { @url = URI.parse(actual) }.to_not raise_error
  expect(actual).to match(Regexp.new(pattern))
end

Then(/^the JSON "([^"]*)" should be an array with (\d+) elements$/) do |jsonpath, n|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect(actual).to be_an(Array)
  puts @json unless actual.size == n.to_i
  expect(actual.size).to eq(n.to_i)
end

Then(/^the JSON "([^"]*)" should be a hash with (\d+) elements$/) do |jsonpath, n|
  actual = JsonPath.on(@json, jsonpath)[0]
  expect(actual).to be_a(Hash)
  puts @json unless actual.size == n.to_i
  expect(actual.size).to eq(n.to_i)
end

Then(/^the response should be JSON:$/) do |json|
  @json = JSON.parse(@browser.last_response.body)
  puts JSON.pretty_generate(@json) if CUCUMBER_VERBOSE
  expected = JSON.parse(json)
  expect(@json).to eq(expected)
end

Then(/^the response "([^"]+)" header should be "([^"]*)"$/) do |header, val|
  expect(@browser.last_response.headers[header]).to eq(val)
end

Given(/^that today's date is "([^"]*)"$/) do |date|
  @today = Date.parse(date)
end

Given(/^the following `([[:alpha:]]+)`s:$/) do |collection_name, table|
  class_name = "Utransact::#{collection_name.singularize.classify}"
  klazz = class_name.constantize
  klazz.all.each(&:destroy)
  # table is a table.hashes.keys # => [:id, :name]
  table.hashes.each do |attributes|
    o = klazz.create! attributes
    raise o.errors.messages.first unless o.errors.empty?
  end
end

And(/^the JSON id should be "([^"]*)"$/) do |expected|
  actual = JsonPath.on(@json, 'data.id')[0]
  # rubocop:disable Security/Eval
  expect(actual).to eq(eval(expected))
  # rubocop:enable Security/Eval
end

And(/^we wait (\d+) second$/) do |n|
  sleep(n.to_i)
end

Then(/^the JSON "([^"]*)" should be a boolean "([^"]*)"$/) do |jsonpath, expected|
  actual = JsonPath.on(@json, jsonpath)[0]
  # rubocop:disable Security/Eval
  expect(actual).to eq(eval(expected))
  # rubocop:enable Security/Eval
end
