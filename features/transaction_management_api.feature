Feature: Transaction Management
  In order to manage the transactions

  @transactions
  Scenario: Get Transaction of 1132233
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |queued|deposit:22322:50000, withdrawal:22322:50000|
      |1132234|53         |done  |deposit:22322:50000                        |
      |1132235|53         |done  |deposit:22322:50000                        |
    When we do GET "/api/v1/transactions?user_id=1132233"
    Then the response status should be "200 (Ok)"
    And the response body should contain a JSON payload
    And the JSON "data.type" should be "Transaction"
    And the JSON "data.attributes.branch_code" should be "53"
    And the JSON "data.attributes.number" should be 1
    And the JSON "data.attributes.user_id" should be "1132233"
    And the JSON "data.attributes.status" should be "queued"
    And the JSON "data.attributes.actions" should be an array with 2 elements

  @transactions
  Scenario: Unsuccessfully create a transaction
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |queued|deposit:22322:50000, withdrawal:22322:50000|
    And the following JSON payload:
    """
      {
        "user_id": "1132233",
        "branch_code": "53",
        "actions": [
          {
            "amount": 50000,
            "account_number": "123123123",
            "type": "deposit"
          }
        ]
      }
    """
    When we do POST "/api/v1/transactions?user_id=1132233"
    Then the response status should be "400 (Error)"
    And the response body should contain a JSON payload
    And the JSON "errors" should be "You have an existing transaction"

  @transactions
  Scenario: Successfully create a transaction
    And the following JSON payload:
    """
      {
        "user_id": "1132233",
        "branch_code": "53",
        "actions": [
          {
            "amount": 50000,
            "account_number": "123123123",
            "type": "deposit"
          }
        ]
      }
    """
    When we do POST "/api/v1/transactions?user_id=1132233"
    Then the response status should be "201 (Created)"
    And the number of transactions is now "1"

  @transactions
  Scenario: Cancel a transaction
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |queued|deposit:22322:50000, withdrawal:22322:50000|
    And the following JSON payload:
    """
      {
        "user_id": "1132233"
      }
    """
    When we do POST "/api/v1/transactions/1132233/cancel"
    Then the response status should be "201 (Created)"
    And the number of transactions is now "0"

  @transactions
  Scenario: Process a transaction
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |queued|deposit:22322:50000, withdrawal:22322:50000|
    And the following JSON payload:
    """
      {
        "user_id": "1132233"
      }
    """
    When we do POST "/api/v1/transactions/1132233/process"
    Then the response status should be "201 (Created)"
    And the number of transactions is now "1"
    And the transaction of "23"'s status is now "transacting"

  @transactions
  Scenario: Finalize a transaction
    Given the following Transactions:
      |user_id|branch_code|status     |actions                                    |
      |1132233|53         |transacting|deposit:22322:50000, withdrawal:22322:50000|
    When we do POST "/api/v1/transactions/1132233/finalize"
    And the response body should contain a JSON payload
    Then the response status should be "201 (Created)"
    And the number of transactions is now "0"
    And the transaction of "23"'s status is now "done"

  @transactions
  Scenario: Add a transaction
    Given the following Transactions:
      |user_id|branch_code|status|actions                                    |
      |1132233|53         |queued|deposit:22322:50000, withdrawal:22322:50000|
    And the following JSON payload:
    """
      {
        "user_id": "1132233",
        "actions": [
          {
            "type": "encashment",
            "account_number": "1231231",
            "amount": 1000
          }
        ]
      }
    """
    When we do POST "/api/v1/transactions/1132233/add"
    Then the response status should be "201 (Created)"
    And the number of transactions is now "1"
    And the transaction of "23"'s status is now "queued"
    And the transaction of "23"'s actions is now "3"
