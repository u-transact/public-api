# frozen_string_literal: true

require 'rubygems'
require 'bundler'

Bundler.require

$LOAD_PATH.unshift File.expand_path('../app', __FILE__)
$LOAD_PATH.unshift File.expand_path('../app/models', __FILE__)

puts "ENV['RACK_ENV'] => #{ENV['RACK_ENV']}"

RACK_ENV = ENV['RACK_ENV'] || 'production'
if RACK_ENV == 'development'
  require 'rack/cors'

  use Rack::Cors do
    allow do
      origins '', 'localhost:3000', '127.0.0.1:3000'

      # headers to expose
      resource '*',
               methods: %i[get post delete put patch options head],
               headers: :any,
               expose: %w[X-User-Authentication-Token X-User-Id],
               max_age: 600
    end
  end
end

puts "RACK_ENV => #{RACK_ENV}"

require 'app'
require 'utransact/branch_management'
require 'utransact/transaction_management'
require 'utransact/transaction_reports_management'

run Rack::Cascade.new [Utransact::BranchManagement, Utransact::TransactionManagement,
                       Utransact::TransactionReportsManagement]
