# frozen_string_literal: true

require 'utransact/branch'

# Test for Utransact::Branch and classes
RSpec.describe Utransact::Branch do
  before do
    Utransact::Branch.delete_all
  end

  it 'can create a branch' do
    Utransact::Branch.create! name: 'Ayala Branch', code: '54', email: 'test@gmail.com',
                              address: 'Ayala cor Gil Puyat', latitude: 14.611772, longitude: 121.075268,
                              contact_number: '09336383736'
    branch = Utransact::Branch.find_by_code('54')
    expect(branch).to_not be_nil
    expect(branch.name).to eq('Ayala Branch')
    expect(branch.code).to eq('54')
  end

  it 'should compute the nearest branch' do
    Utransact::Branch.create! name: 'Ayala Branch', code: '55', email: 'test@gmail.com',
                              address: 'Ayala cor Gil Puyat', latitude: 14.611772, longitude: 121.075268,
                              contact_number: '09336383736'
    Utransact::Branch.create! name: 'Ayala Branch 2', code: '54', email: 'test2@gmail.com',
                              address: 'Ayala cor Gil Puyat 2', latitude: 14.953611, longitude: 120.888889,
                              contact_number: '09336383733'
    value = Utransact::Branch.find_nearest(14.589853, 121.060939)
    expect(value.length).to eq(2)
  end
end
