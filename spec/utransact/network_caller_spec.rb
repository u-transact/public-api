# frozen_string_literal: true

require 'network_caller'

# Test for NetworkCaller classes
RSpec.describe NetworkCaller do
  it 'can retrieve the branches' do
    branches = NetworkCaller.list_branches
    expect(branches).to_not be_nil
  end
end
