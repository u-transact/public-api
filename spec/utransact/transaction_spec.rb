# frozen_string_literal: true

require 'utransact/transaction'
require 'redis'

# Test for Utransact::Transaction and classes
RSpec.describe Utransact::Transaction do
  before do
    Utransact::Transaction.delete_all
    redis = Redis.new(Settings.redis.to_h)
    redis.flushall
  end

  it 'can generate a transaction number' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312'
    expect(transaction.number).to_not be(nil)
    expect(transaction.number).to eq(1)
  end

  it 'can create a transaction actions' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312'
    transaction.actions.create! type: 'deposit', amount: 50_000, account_number: '223123123123'
    expect(transaction.number).to_not be(nil)
    expect(transaction.number).to eq(1)
    expect(transaction.actions).to_not be(nil)
    expect(transaction.actions.length).to be(1)
  end

  it 'can create more than 1 transaction actions' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312'
    transaction.actions.create! type: 'deposit', amount: 50_000, account_number: '223123123123'
    transaction.actions.create! type: 'deposit', amount: 30_000, account_number: '111111111111'
    expect(transaction.number).to_not be(nil)
    expect(transaction.number).to eq(1)
    expect(transaction.actions).to_not be(nil)
    expect(transaction.actions.length).to be(2)
  end

  it 'should generate per transaction stats' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312'
    transaction.actions.create! type: 'deposit', amount: 20_000, account_number: '223123123123'
    transaction.actions.create! type: 'deposit', amount: 40_000, account_number: '111111111111'
    sleep(2)
    transaction.status = 'done'
    transaction.save
    transaction2 = Utransact::Transaction.create! branch_code: '53', user_id: '12312313'
    transaction2.actions.create! type: 'payment', amount: 10_000, account_number: '123123'
    transaction2.actions.create! type: 'withdrawal', amount: 30_000, account_number: '111111111111'
    sleep(5)
    transaction2.status = 'done'
    transaction2.save
    result = Utransact::Transaction.generate_per_transaction_stats
    transactions = result[0]
    overall_stats = result[1]
    expect(overall_stats[:total_amount]).to eq(40_000.0)
    expect(overall_stats[:total_transactions]).to eq(4)
    expect(overall_stats[:total_average_queue_time]).to_not be_nil
    expect(transactions[:payment][:total]).to eq(10_000.0)
    expect(transactions[:withdrawal][:total]).to eq(30_000.0)
    expect(transactions[:deposit][:total]).to eq(60_000.0)
    expect(transactions[:encashment][:total]).to eq(0.0)
  end

  it 'should compute the overall stats for today' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312'
    transaction.actions.create! type: 'deposit', amount: 20_000, account_number: '223123123123'
    transaction.actions.create! type: 'deposit', amount: 40_000, account_number: '111111111111'
    sleep(2)
    transaction.status = 'done'
    transaction.save
    transaction2 = Utransact::Transaction.create! branch_code: '53', user_id: '12312313'
    transaction2.actions.create! type: 'payment', amount: 10_000, account_number: '123123'
    transaction2.actions.create! type: 'withdrawal', amount: 30_000, account_number: '111111111111'
    sleep(5)
    transaction2.status = 'done'
    transaction2.save
    result = Utransact::Transaction.compute_total_today
    expect(result[:total_amount]).to eq(40_000.0)
    expect(result[:total_transactions]).to eq(4)
    expect(result[:total_average_queue_time]).to_not be_nil
  end

  it 'should compute the hourly transactions' do
    transaction = Utransact::Transaction.create! branch_code: '53', user_id: '12312312', status: 'done'
    transaction.actions.create! type: 'deposit', amount: 20_000, account_number: '223123123123'
    transaction.actions.create! type: 'deposit', amount: 40_000, account_number: '111111111111'
    transaction2 = Utransact::Transaction.create! branch_code: '53', user_id: '12312313', status: 'done'
    transaction2.actions.create! type: 'payment', amount: 10_000, account_number: '123123'
    transaction2.actions.create! type: 'withdrawal', amount: 30_000, account_number: '111111111111'
    result = Utransact::Transaction.hourly
    expect(result.length).to eq(24)
    expect(result[0][:hour]).to eq('00:00')
    expect(result[0][:amount]).to eq(0)
  end
end
